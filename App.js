import React, { useEffect } from 'react';
import { View, Text, Alert } from 'react-native';
import axios from 'axios';

const App = () => {

  const callAPI = async () => {
    try {
      const result = await axios.get('https://10.0.101.21:5001/weatherforecast');
      console.log(result);
    } catch (e) {
      Alert.alert(e.message);
    }
  }

  useEffect(() => {
    callAPI();
  }, [])

  return (
    <View>
      <Text>Hello</Text>
    </View>
  )
}

export default App;